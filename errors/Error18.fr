package errors.Error18 where  -- classes errors

{-
E errors/Error18.fr:24: Class variable a is used with different kinds.
E errors/Error18.fr:26: Kind of class variable a is 0 in XEQ.==
E errors/Error18.fr:25: Kind of class variable a is 1 in XEQ.eqqq
E errors/Error18.fr:26: class member == must not be annotated.
E errors/Error18.fr:33: class variable o does not occur in type of class member
                ohh
E errors/Error18.fr:35: class member anno must be annotated
E errors/Error18.fr:34: class variable o must not be constrained in type of
                class member upps
E errors/Error18.fr:30: definition of class member value `L.length` clashes with
                class member value `Length.length`, please use another name
E errors/Error18.fr:20: Class variable k is used with different kinds.
E errors/Error18.fr:21: Kind of class variable k is 1 in Kind.foo
E errors/Error18.fr:22: Kind of class variable k is 2 in Kind.bar
 -}

class Kind k where
    foo :: k a
    bar :: k -> k a b

class XEQ Eq a => a where
    eqqq :: a Int -> a String -> Bool
    (==) :: a -> a -> Bool
    a == b = true

class L a where
    length :: a -> Int

class Occur o where
    ohh :: Int
    upps :: Eq o => o -> Bool
    anno 42 = "bad"

-- class A B a => a
-- class B A b => b
