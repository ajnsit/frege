{--
    Provide functions for use in the fregIDE
-}

{-
 * $Author$
 * $Revision$
 * $Date$
 * $Id$
-}

package frege.compiler.EclipseUtil where

import frege.compiler.Data
import frege.compiler.Utilities as U(isPSigma)
import frege.List (each, values)
import Data.List as DL(sortBy)


--- This is $Revision$
public version = v "$Revision$" where
    v (m ~ #(\d+)#) | Just g <- m.group 1 = g.atoi
    v _ = 0

{--
    Create a list of triples with  position, namespace and package
    for every import except the automatic ones.
    -}
imports :: Global -> [(Position, String, String)]
imports g = [ (pos, NSName.unNS ns, Pack.un pack) | 
                (ns, pos) <- (sortBy (comparing snd) • each) g.sub.nsPos,
                pos != Position.null,
                pack <- maybe [] (:[]) (g.namespaces.lookup ns) ]

{--
    Create the list of symbols ordered by position
    -}
symbols :: Symtab -> [Symbol]
symbols tab = (sortBy positionAndName • filter wanted • values) tab
    where
        positionAndName a b = case Symbol.pos a <=> Symbol.pos b of
                                    Eq -> comparing (QName.base • Symbol.name) a b
                                    ne -> ne 
        wanted :: Symbol -> Bool 
        wanted sym 
            | sym.{alias?}                       = false
            | Local{} <- sym.name                = true
            -- sym.vis == Private                 = false
            | sym.name.base ~ ´^(chg|upd|has|let|anon|lc)\$´ = false
            | otherwise                          = true

exprSymbols = U.foldEx false collectsyms []
    where
        collectsyms acc Let{env} = do
            syms <- mapSt U.findV env
            stio (Left (acc ++ syms))
        collectsyms acc _        = stio (Left acc)

verbose g t
    | isPSigma t = "?"
    | otherwise    = t.rho.nicer g

{--
    Make a label for a symbol
    -}
label g SymI{clas,typ} = clas.nicer g ++ "  "   ++ verbose g typ
label g SymV{name,typ} = name.base    ++ " :: " ++ verbose g typ 
label g SymD{name,typ} = name.base    ++ " :: " ++ verbose g typ             
label g sym            = sym.name.base 
                    
{--
    Increment the pass number in the state
    -}
passDone = changeST Global.{sub <- SubSt.{nextPass <- (1+)}}    

{--
    Failure tolerant version of 'Global.thisTab' for use in TreeModelBuilder.
    In case of syntax errors, there is no symtab yet, hence Global.thisTab
    is undefined. This, in turn, causes an exception in Eclipse. We can avoid
    this by just pretending the symbol table was empty.
    -}
thisTab :: Global -> Symtab
thisTab g = case g.packages.lookup g.thisPack of
        Just st -> st
        Nothing -> Symtab.Nil
    